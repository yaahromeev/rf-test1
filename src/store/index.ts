import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'
import { RootState } from './types';
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const store: StoreOptions<RootState> = {
  state: {
    bookmarkedRepos: [],
  },
  getters: {
    BOOKMARKS: state => state.bookmarkedRepos
  },
  mutations: {
    ADD_BOOKMARK: (state, payload) => {
      state.bookmarkedRepos.push(payload)
    },
    REMOVE_BOOKMARK: (state, payload) => {
      state.bookmarkedRepos = state.bookmarkedRepos.filter(obj => obj.id !== payload.id)
    }
  },
  actions: {
    SAVE_BOOKMARK: (context, payload) => {
      context.commit('ADD_BOOKMARK', payload)
    },
    DELETE_BOOKMARK: (context, payload) => {
      context.commit('REMOVE_BOOKMARK', payload)
    }
  },
  modules: {

  },
  plugins: [
    createPersistedState()
  ]
}

export default new Vuex.Store<RootState>(store);